module postalcodelookup {
    requires javafx.controls;
    requires javafx.fxml;

    exports ntnu.edu.idatt2001.mappe3.postalcodelookup;
    exports ntnu.edu.idatt2001.mappe3.postalcodelookup.controllers to javafx.fxml;
    exports ntnu.edu.idatt2001.mappe3.postalcodelookup.controllers.controls to javafx.fxml;
    exports ntnu.edu.idatt2001.mappe3.postalcodelookup.models to javafx.fxml;

    opens ntnu.edu.idatt2001.mappe3.postalcodelookup.controllers to javafx.fxml;
    opens ntnu.edu.idatt2001.mappe3.postalcodelookup.models to javafx.base;
}
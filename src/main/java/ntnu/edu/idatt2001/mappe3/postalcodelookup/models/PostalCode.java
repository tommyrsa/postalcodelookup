package ntnu.edu.idatt2001.mappe3.postalcodelookup.models;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Represents a postal code.
 */
public class PostalCode {

    private final String zipCode;
    private final String cityName;
    private final String municipalityName;

    /**
     * Constructor.
     * Initializes a new {@code PostalCode} object and checks for invalid inputs.
     *
     * @param zipCode           Zip code for the postal code.
     * @param cityName          Name of the city associated with the postal code.
     * @param municipalityName  Name of the municipality the city belongs to.
     */
    public PostalCode(String zipCode, String cityName, String municipalityName) {
        Objects.requireNonNull(zipCode);
        Objects.requireNonNull(cityName);
        Objects.requireNonNull(municipalityName);

        if (zipCode.trim().isEmpty()) {
            throw new IllegalArgumentException("Argument `zipCode` cannot be empty.");
        }

        if (cityName.trim().isEmpty()) {
            throw new IllegalArgumentException("Argument `zipCode` cannot be empty.");
        }

        if (municipalityName.trim().isEmpty()) {
            throw new IllegalArgumentException("Argument `zipCode` cannot be empty.");
        }

        if (!isValidZipCode(zipCode)) {
            throw new IllegalArgumentException("Argument `zipCode` is an invalid zip code.");
        }

        this.zipCode = zipCode;
        this.cityName = cityName;

        this.municipalityName = municipalityName;
    }

    /**
     * @return Zip code for the postal code.
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * @return Name of the city the postal code is associated with.
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * @return Name of the municipality the postal code is associated with.
     */
    public String getMunicipalityName() {
        return municipalityName;
    }

    /**
     * Checks whether a given zip code matches a zip code regex.
     *
     * @param zipCode Zip code to check.
     * @return {@code true} if {@code zipCode} matches a zip code regex. {@code false} if not.
     */
    private boolean isValidZipCode(String zipCode) {
        final Pattern ZIP_CODE_PATTERN = Pattern.compile("([0-9]{4})");

        return ZIP_CODE_PATTERN.matcher(zipCode).matches();
    }

    /**
     * Override of {@code Object}'s {@code equals()} method.
     *
     * @param other Object to check equality of, compared to the initiator.
     * @return {@code true} if the objects have equal zip code. {@code false} otherwise.
     */
    @Override
    public boolean equals(Object other) {
        if (other == this) return true;

        if (!(other instanceof PostalCode)) return false;

        PostalCode otherPostalCode = (PostalCode) other;
        return this.zipCode.equals(otherPostalCode.zipCode);
    }

    /**
     * Override of {@code Object}'s {@code hashCode()} method.
     *
     * @return Hash code of zip code associated with this postal code.
     */
    @Override
    public int hashCode() {
        return zipCode.hashCode();
    }
}

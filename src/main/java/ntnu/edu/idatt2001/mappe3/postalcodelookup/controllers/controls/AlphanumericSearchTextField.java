package ntnu.edu.idatt2001.mappe3.postalcodelookup.controllers.controls;

import javafx.beans.Observable;
import javafx.scene.control.TextField;
import ntnu.edu.idatt2001.mappe3.postalcodelookup.models.SearchType;

import java.util.Objects;

/**
 * Custom search text field control that differentiates between alphabetical and numerical searches.
 */
public class AlphanumericSearchTextField extends TextField {

    /**
     * Search event functional interface for setting search handlers.
     */
    @FunctionalInterface
    public interface SearchEvent {
        /**
         * Event firer.
         *
         * @param searchType Determines whether search was alphabetical or numerical.
         * @param query Search query.
         */
        void fire(SearchType searchType, String query);
    }

    private SearchEvent onSearchEvent;

    /**
     * Constructor.
     * Calls JavaFX's {@code TextField} constructor,
     * sets default search event handler
     * and adds listener for super's {@code StringProperty}.
     */
    public AlphanumericSearchTextField() {
        super();
        onSearchEvent = (a, b) -> { throw new IllegalStateException("Event handler `onSearchEvent must be set`."); };
        super.textProperty().addListener(this::onTextChanged);
    }

    /**
     * Sets the search event handler for this control.
     * Event is fired upon any text input.
     *
     * @param onSearchEvent Event handler.
     */
    public final void setOnSearchEvent(SearchEvent onSearchEvent) {
        Objects.requireNonNull(onSearchEvent);

        this.onSearchEvent = onSearchEvent;
    }

    /**
     * Listener for {@code TextField}'s {@code StringProperty}.
     *
     * @param observable Event context - unused.
     */
    private void onTextChanged(Observable observable) {
        SearchType searchType;
        String query = super.getText();

        searchType = isInteger(query)
                    ? SearchType.NUMERIC
                    : SearchType.ALPHABETIC;

        onSearchEvent.fire(searchType, query);
    }

    /**
     * Helper for determining whether a string is an integer.
     *
     * @param integerAsString String to check.
     * @return {@code true} if the string may be parsed to an integer. {@code false} otherwise.
     */
    protected boolean isInteger(String integerAsString) {
        try {
            Integer.parseInt(integerAsString);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}

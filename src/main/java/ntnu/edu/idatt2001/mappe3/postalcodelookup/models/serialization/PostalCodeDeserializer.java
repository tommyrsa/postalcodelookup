package ntnu.edu.idatt2001.mappe3.postalcodelookup.models.serialization;

import ntnu.edu.idatt2001.mappe3.postalcodelookup.Resource;
import ntnu.edu.idatt2001.mappe3.postalcodelookup.models.PostalCode;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Deserializer for postal codes as supplied by {@literal Bring}.
 */
public class PostalCodeDeserializer {

    private final File dataSource;

    /**
     * Constructor.
     * Creates a {@code File} object to be used by deserializer from a program resource.
     *
     * @param dataResource {@code Resource} to use as data source.
     */
    public PostalCodeDeserializer(Resource dataResource) {
        Objects.requireNonNull(dataResource);

        try {
            dataSource = new File(dataResource.getResourceURL().toURI());
        } catch (URISyntaxException ignored) {
            // Impossible for URI syntax to be malformed, as it is generated from a constant
            throw new RuntimeException();
        }
    }

    /**
     * Deserializes data from resource given in constructor.
     *
     * @see #PostalCodeDeserializer(Resource)
     * @return Deserialized data.
     */
    public List<PostalCode> deserialize() {
        List<PostalCode> deserializedData = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(dataSource, StandardCharsets.UTF_8))) {
            reader.lines().forEach(line -> {
                String[] fields = line.split("\t");
                deserializedData.add(new PostalCode(fields[0], fields[1], fields[3]));
            });
        } catch (IOException e) {
            // Data file will always be present in jar file, otherwise a test will catch this issue.
            // However, we should throw an exception in case the user is to remove the file from the jar,
            // or in case they lock it by opening it.
            String exceptionMessage = "Error reading from data source: ";
            if (!dataSource.exists()) {
                exceptionMessage += "File not found.";
            } else if (!dataSource.canRead()) {
                exceptionMessage += "Cannot read file.";
            }

            throw new RuntimeException(exceptionMessage);
        }

        return deserializedData;
    }
}

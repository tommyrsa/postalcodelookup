package ntnu.edu.idatt2001.mappe3.postalcodelookup.models;

/**
 * Represents a search type that may be either alphabetical or numerical.
 */
public enum SearchType {
    NUMERIC,
    ALPHABETIC
}

package ntnu.edu.idatt2001.mappe3.postalcodelookup;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ntnu.edu.idatt2001.mappe3.postalcodelookup.factories.SceneFactory;

/**
 * Main JavaFX application class.
 */
public class App extends Application {

    private final static String TITLE = "Postal Code Lookup";

    /**
     * Main entry point of application.
     * Launches a concrete JavaFX application from a static context.
     *
     * @param args Command line arguments - unused.
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Override of JavaFX {@code Application}'s {@code start()} method.
     * Creates the main scene for the application and displays the JavaFX injected {@code Stage}.
     * 
     * @see Application#start(Stage) 
     * @param primaryStage Primary {@code Stage} object to display - injected by JavaFX.
     */
    @Override
    public void start(Stage primaryStage) {
        Scene mainScene = new SceneFactory().createScene(Resource.MAIN_FXML_VIEW);

        primaryStage.setScene(mainScene);
        primaryStage.setTitle(TITLE);
        primaryStage.show();
        primaryStage.toFront();
    }

    /**
     * Encapsulates private constant {@code TITLE}.
     *
     * @return Title of the application.
     */
    public static String getTitle() {
        return TITLE;
    }
}

package ntnu.edu.idatt2001.mappe3.postalcodelookup;

import java.net.URL;
import java.util.Objects;

/**
 * Enum for ease of both access to- and testing of resources.
 */
public enum Resource {
    MAIN_FXML_VIEW("views/MainView.fxml"),
    POSTAL_CODE_REGISTER("postal-code-register-ansi.txt"),
    STYLESHEET("style.css");

    private final String resourcePath;

    /**
     * Constructor for resources.
     *
     * @param resourcePath Path for resource, assuming base path of
     *                      {@literal resources/ntnu/idatt2001/mappe3/postalcodelookup}.
     */
    Resource(String resourcePath) {
        Objects.requireNonNull(resourcePath);

        this.resourcePath = resourcePath;
    }

    /**
     * @return URL for resource.
     */
    public URL getResourceURL() {
        return App.class.getResource(resourcePath);
    }

    public String getResourceFileExtension() {
        return resourcePath.substring(resourcePath.indexOf('.') + 1);
    }
}

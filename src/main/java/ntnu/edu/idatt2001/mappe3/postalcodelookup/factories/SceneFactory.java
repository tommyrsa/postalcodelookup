package ntnu.edu.idatt2001.mappe3.postalcodelookup.factories;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import ntnu.edu.idatt2001.mappe3.postalcodelookup.Resource;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

/**
 * Factory class for JavaFX {@code Scene}s.
 */
public class SceneFactory {

    /**
     * Creates and returns a JavaFX {@code Scene} based on the FXML resource.
     *
     * @param fxmlResource FXML {@code Resource} object to create {@code Scene} from.
     * @return JavaFX {@code Scene} object.
     */
    public Scene createScene(Resource fxmlResource) {
        Objects.requireNonNull(fxmlResource);

        if (!fxmlResource.getResourceFileExtension().equals("fxml")) {
            throw new IllegalArgumentException("Resource is not an FXML resource.");
        }

        FXMLLoader fxmlLoader = new FXMLLoader(fxmlResource.getResourceURL());

        Parent root;
        try {
            root = fxmlLoader.load();
        } catch (IOException e) {
            // FXML file will always be present in jar file, otherwise a test will catch this issue.
            // However, we should throw an exception in case the user is to remove the file from the jar.
            throw new RuntimeException("Missing file: " + fxmlResource.getResourceURL());
        }

        Scene scene = new Scene(root);

        URL styleSheetURL = Resource.STYLESHEET.getResourceURL();
        scene.getStylesheets().add(styleSheetURL.toExternalForm());

        return scene;
    }
}

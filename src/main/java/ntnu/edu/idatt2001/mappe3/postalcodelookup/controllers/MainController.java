package ntnu.edu.idatt2001.mappe3.postalcodelookup.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import ntnu.edu.idatt2001.mappe3.postalcodelookup.App;
import ntnu.edu.idatt2001.mappe3.postalcodelookup.Resource;
import ntnu.edu.idatt2001.mappe3.postalcodelookup.controllers.controls.AlphanumericSearchTextField;
import ntnu.edu.idatt2001.mappe3.postalcodelookup.models.PostalCode;
import ntnu.edu.idatt2001.mappe3.postalcodelookup.models.SearchType;
import ntnu.edu.idatt2001.mappe3.postalcodelookup.models.serialization.PostalCodeDeserializer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Controller for {@literal MainView}.
 */
public class MainController {
    // Data members
    private List<PostalCode> allPostalCodes;

    // FXML view fields
    @FXML private Label titleLabel;
    @FXML private AlphanumericSearchTextField searchTextField;
    @FXML private TableView<PostalCode> postalCodeTableView;
    @FXML private TableColumn<PostalCode, String> zipCodeColumn;
    @FXML private TableColumn<PostalCode, String> cityColumn;
    @FXML private TableColumn<PostalCode, String> municipalityColumn;

    /**
     * JavaFX initialization of controller.
     */
    @FXML
    private void initialize() {
        titleLabel.setText(App.getTitle());

        setColumnCellValueFactories();
        searchTextField.setOnSearchEvent(this::onSearchAction);

        loadData();
        setTableData(allPostalCodes);
    }

    // Listeners

    /**
     * Event handler for search queries, fired by {@code searchTextField} member.
     *
     * @param searchType Type of search (alphabetical or numerical) - injected by {@code SearchTextField}.
     * @param query Search query - injected by {@code SearchTextField}.
     */
    private void onSearchAction(SearchType searchType, String query) {
        if (query.trim().isEmpty()) {
            setTableData(allPostalCodes);
        }

        switch (searchType) {
            case NUMERIC:
                searchTableByZipCode(query);
                break;
            case ALPHABETIC:
                searchTableByCity(query);
                break;
        }
    }

    // Helpers

    /**
     * Helper for searching {@code postalCodeTableView} member's data by a zip code.
     *
     * @param zipCode Zip code to search for.
     */
    private void searchTableByZipCode(String zipCode) {
        filterTable(postalCode -> postalCode.getZipCode().contains(zipCode));
    }

    /**
     * Helper for searching {@code postalCodeTableView} member's data by a city name.
     *
     * @param cityName City name to search for.
     */
    private void searchTableByCity(String cityName) {
        filterTable(postalCode -> postalCode
                                    .getCityName().toLowerCase()
                                    .contains(cityName.toLowerCase()));
    }

    /**
     * Helper for filtering {@code postalCodeTableView} member's data by a predicate.
     *
     * @param predicate Predicate to filter data by.
     */
    private void filterTable(Predicate<PostalCode> predicate) {
        List<PostalCode> foundEntries = allPostalCodes
                                        .stream()
                                        .filter(predicate)
                                        .collect(Collectors.toList());

        setTableData(foundEntries);
    }

    /**
     * Helper for setting the data of {@code postalCodeTableView} member.
     *
     * @param data Data to set.
     */
    private void setTableData(List<PostalCode> data) {
        postalCodeTableView.getItems().setAll(data);
    }

    /**
     * Helper for loading data from a register into {@code allPostalCodes} member.
     */
    private void loadData() {
        PostalCodeDeserializer deserializer = new PostalCodeDeserializer(Resource.POSTAL_CODE_REGISTER);
        List<PostalCode> data = deserializer.deserialize();

        allPostalCodes = new ArrayList<>(data);
    }

    /**
     * Helper for setting the column cell value factories of all {@code TableColumn}s in this class.
     */
    private void setColumnCellValueFactories() {
        zipCodeColumn.setCellValueFactory(new PropertyValueFactory<>("zipCode"));
        cityColumn.setCellValueFactory(new PropertyValueFactory<>("cityName"));
        municipalityColumn.setCellValueFactory(new PropertyValueFactory<>("municipalityName"));
    }
}

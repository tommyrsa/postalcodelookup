package ntnu.edu.idatt2001.mappe3.postalcodelookup.models.serialization;

import ntnu.edu.idatt2001.mappe3.postalcodelookup.Resource;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PostalCodeDeserializerTest {

    @Nested
    public class ConstructorTest {
        @Test
        public void constructor_argIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class,
                () -> new PostalCodeDeserializer(null));
        }

        @Test
        public void constructor_argIsResource_throwsNothing() {
            assertDoesNotThrow(() -> new PostalCodeDeserializer(Resource.POSTAL_CODE_REGISTER));
        }
    }

    @Test
    public void deserialize_deserializesData() {
        PostalCodeDeserializer deserializer = new PostalCodeDeserializer(Resource.POSTAL_CODE_REGISTER);

        assertNotEquals(0, deserializer.deserialize().size());
    }
}

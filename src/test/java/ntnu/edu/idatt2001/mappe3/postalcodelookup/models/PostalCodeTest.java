package ntnu.edu.idatt2001.mappe3.postalcodelookup.models;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PostalCodeTest {

    @Nested
    public class ConstructorTest {

        @Test
        public void constructor_zipCodeIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class,
                () -> new PostalCode(null, "test", "test"));
        }

        @Test
        public void constructor_zipCodeIsEmpty_throwsIllegalArgumentException() {
            assertThrows(IllegalArgumentException.class,
                () -> new PostalCode("", "test", "test"));
            assertThrows(IllegalArgumentException.class,
                () -> new PostalCode("       ", "test", "test"));
        }

        @Test
        public void constructor_zipCodeIsInvalidZipCode_throwsIllegalArgumentException() {
            assertThrows(IllegalArgumentException.class,
                () -> new PostalCode("1", "test", "test"));
            assertThrows(IllegalArgumentException.class,
                () -> new PostalCode("01", "test", "test"));
            assertThrows(IllegalArgumentException.class,
                () -> new PostalCode("001", "test", "test"));
            assertThrows(IllegalArgumentException.class,
                () -> new PostalCode("00001", "test", "test"));
        }

        @Test
        public void constructor_cityNameIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class,
                () -> new PostalCode("0001", null, "test"));
        }

        @Test
        public void constructor_cityNameIsEmpty_throwsIllegalArgumentException() {
            assertThrows(IllegalArgumentException.class,
                () -> new PostalCode("0001", "", "test"));
            assertThrows(IllegalArgumentException.class,
                () -> new PostalCode("0001", "       ", "test"));
        }

        @Test
        public void constructor_municipalityNameIsNull_throwsNullPointerException() {
            assertThrows(NullPointerException.class,
                () -> new PostalCode("0001", "test", null));
        }

        @Test
        public void constructor_municipalityNameIsEmpty_throwsIllegalArgumentException() {
            assertThrows(IllegalArgumentException.class,
                () -> new PostalCode("0001", "test", ""));
            assertThrows(IllegalArgumentException.class,
                () -> new PostalCode("0001", "test", "           "));
        }

        @Test
        public void constructor_allArgsAreValid_throwsNothing() {
            assertDoesNotThrow(() -> new PostalCode("0001", "test", "test"));
        }
    }

    @Nested
    public class GetterTest {
        private final String TEST_ZIP_CODE = "0001";
        private final String TEST_CITY_NAME = "Test City";
        private final String TEST_MUNICIPALITY_NAME = "Test County";

        private final PostalCode TEST_POSTAL_CODE
            = new PostalCode(TEST_ZIP_CODE, TEST_CITY_NAME, TEST_MUNICIPALITY_NAME);

        @Test
        public void getZipCode_returnsZipCode() {
            assertEquals(TEST_ZIP_CODE, TEST_POSTAL_CODE.getZipCode());
        }

        @Test
        public void getCityName_returnsCityName() {
            assertEquals(TEST_CITY_NAME, TEST_POSTAL_CODE.getCityName());
        }

        @Test
        public void getMunicipalityName_returnsMunicipalityName() {
            assertEquals(TEST_MUNICIPALITY_NAME, TEST_POSTAL_CODE.getMunicipalityName());
        }
    }
}

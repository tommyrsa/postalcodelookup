package ntnu.edu.idatt2001.mappe3.postalcodelookup;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ResourceTest {
    @ParameterizedTest
    @EnumSource(Resource.class)
    public void allResourcesExist(Resource resource) {
        assertNotNull(resource.getResourceURL());
    }

}

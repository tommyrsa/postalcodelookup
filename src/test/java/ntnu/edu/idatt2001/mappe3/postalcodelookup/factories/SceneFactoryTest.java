package ntnu.edu.idatt2001.mappe3.postalcodelookup.factories;

import javafx.application.Platform;
import ntnu.edu.idatt2001.mappe3.postalcodelookup.Resource;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SceneFactoryTest {
    private final static SceneFactory SCENE_FACTORY = new SceneFactory();

    @BeforeAll
    public static void beforeAll() {
        // Initialize JavaFX toolkit
        Platform.startup(() -> {});
    }

    @AfterAll
    public static void afterAll() {
        // Close JavaFX toolkit, as it is only required for this unit test
        Platform.exit();
    }

    @Test
    public void createScene_argIsNull_throwsNullPointerException() {
        assertThrows(NullPointerException.class, () -> SCENE_FACTORY.createScene(null));
    }

    @Test
    public void createScene_argIsNotFXMLResource_throwsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> SCENE_FACTORY.createScene(Resource.POSTAL_CODE_REGISTER));
    }

    @Test
    public void createScene_argIsFXMLResource_returnsScene() {
        assertNotNull(SCENE_FACTORY.createScene(Resource.MAIN_FXML_VIEW));
    }
}
